from typing import Tuple
from uuid import UUID

from flask import Request
from werkzeug.exceptions import BadRequest


def parse_auth_request(request: Request) -> Tuple[str, str]:
    """Parse username and password field from json body

    Raises:
        BadRequest: one of the field was not found
    Returns:
        (str, str) -- string representation of username and password
    """
    data = request.get_json()
    username: str = data.get("username", None)
    password: str = data.get("password", None)

    reason = "The request body not contains {} field"
    if not username:
        raise BadRequest(reason.format("username"))
    if not password:
        raise BadRequest(reason.format("password"))

    return username, password


def parse_joke(request: Request) -> str:
    """Parse Joke text from request body

    Raises:
        BadRequest: Not found "text" field

    Returns:
        str -- text of joke
    """
    data = request.get_json()
    joke_text: str = data.get("text", None)
    if not joke_text:
        raise BadRequest("The request body not contains text field")
    return joke_text


def parse_uuid(uuid_candidate: str) -> UUID:
    """[Parse UUID from string

    Raises:
        BadRequest: UUID format is not valid

    Returns:
        UUID -- parsed UUID
    """
    try:
        uuid = UUID(uuid_candidate)
    except ValueError:
        raise BadRequest("Wrong format of joke UUID")
    return uuid
