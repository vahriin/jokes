import logging

from uuid import UUID

from flask import request
from flask_restplus import Resource
from werkzeug.exceptions import BadRequest, Unauthorized
from flask_jwt_extended import verify_fresh_jwt_in_request, get_jwt_identity
from flask_jwt_extended.exceptions import (
    JWTExtendedException,
    InvalidHeaderError,
    FreshTokenRequired,
)
from jwt.exceptions import ExpiredSignatureError

from model.log import Log


class LogResource(Resource):
    def dispatch_request(self, *args, **kwargs):
        try:
            verify_fresh_jwt_in_request()
            uuid: str = get_jwt_identity()
            try:
                uuid_: UUID = UUID(uuid)
                ip: str = request.remote_addr
                logging.info(f"Dispatch request from IP {ip}, user uuid {uuid_}")
                Log.create(user_uuid=uuid_, ip_addr=ip)

                kwargs["user_uuid"] = uuid_
                return super().dispatch_request(*args, **kwargs)
            except (ValueError, TypeError):
                raise BadRequest("Wrong token format")
        except InvalidHeaderError:
            raise BadRequest("No access token found in header")
        except (FreshTokenRequired, ExpiredSignatureError):
            raise BadRequest("Token was expired")
        except JWTExtendedException:
            raise Unauthorized()
