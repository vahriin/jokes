import logging
from uuid import UUID

from flask_restplus import Namespace, fields
from flask import request

from werkzeug.exceptions import NotFound, Conflict

from api.log_resource import LogResource
from api.util import get_joke
from api.parser import parse_joke, parse_uuid
from model.joke import Joke as MJoke
from model.exceptions import AlreadyExistsError, NotFoundError

authorizations = {"apikey": {"type": "apiKey", "in": "header", "name": "X-Auth-Token"}}

jokes_namespace = Namespace(
    name="jokes",
    path="/jokes/",
    description="Jokes operations",
    authorizations=authorizations,
)

joke_id = jokes_namespace.model(
    "Joke UUID",
    {
        "uuid": fields.String(
            required=True,
            descriptions="UUID of created joke",
            example="ffffffff-ffff-ffff-ffff-ffffffffffff",
        )
    },
)

joke_text = jokes_namespace.model(
    "Joke Text",
    {
        "text": fields.String(
            required=True, descriptions="Text of joke", example="This is text of joke"
        )
    },
)

joke = jokes_namespace.inherit("Joke", joke_id, joke_text)


class CustomExceptionTranslator(LogResource):
    def dispatch_request(self, *args, **kwargs):
        try:
            return super().dispatch_request(*args, **kwargs)
        except NotFoundError as e:
            raise NotFound(e)
        except AlreadyExistsError as e:
            raise Conflict(e)


@jokes_namespace.header("Content-Type", "appication/json")
@jokes_namespace.route("/")
@jokes_namespace.doc(security="apikey")
class JokesList(CustomExceptionTranslator):
    """Provide access to jokes list and create jokes by POST request"""

    @jokes_namespace.doc("jokes_list")
    @jokes_namespace.response(400, "Bad request")
    @jokes_namespace.response(401, "Unauthorized")
    @jokes_namespace.marshal_list_with(joke)
    def get(self, user_uuid: UUID):
        """Return jokes that belong to user with that user_uuid.

        Returns:
            str -- JSON list of jokes that belong to this user
        """
        logging.info(f"Get jokes list for user_uuid {user_uuid}")
        return MJoke.get_list(owner=user_uuid)

    @jokes_namespace.doc("create_joke")
    @jokes_namespace.response(400, "Bad request")
    @jokes_namespace.response(401, "Unauthorized")
    @jokes_namespace.marshal_with(joke_id, code=201)
    def post(self, user_uuid: UUID):
        """Create joke by specified data. Authorized user will be owner.

        Returns:
            str -- JSON representation of created joke"""
        joke_text: str = ""
        if request.json:
            logging.info("Creating joke from user data")
            joke_text = parse_joke(request)
        else:
            logging.info("Creating joke from external API")
            joke_text = get_joke()
        joke = MJoke.create(owner=user_uuid, text=joke_text)
        logging.info(f"User with uuid {user_uuid} create joke {joke}")
        return joke, 201


@jokes_namespace.header("Content-Type", "appication/json")
@jokes_namespace.route("/<uuid>")
@jokes_namespace.doc(security="apikey")
class Joke(CustomExceptionTranslator):
    """Provide access to joke with specified ID, changing joke by PUT and deleting by DELETE"""

    @jokes_namespace.doc("get_joke")
    @jokes_namespace.response(400, "Bad request")
    @jokes_namespace.response(404, "Jokes not found")
    @jokes_namespace.response(401, "Unauthorized")
    @jokes_namespace.marshal_with(joke)
    def get(self, uuid: str, user_uuid: UUID):
        """Return JSON representation of Joke with specified UUID.
        Return 404 if that UUID not exists of current user is not owner of
        joke with that UUID

        Returns:
            str -- JSON of joke
        """

        joke_uuid = parse_uuid(uuid)
        return MJoke.get(uuid=joke_uuid, owner=user_uuid)

    @jokes_namespace.doc(body=joke_text)
    @jokes_namespace.doc("change_joke")
    @jokes_namespace.response(200, "Changed")
    @jokes_namespace.response(400, "Bad request")
    @jokes_namespace.response(404, "Joke not found")
    @jokes_namespace.response(401, "Unauthorized")
    @jokes_namespace.expect(joke_text, validation=True)
    def put(self, uuid: str, user_uuid: UUID):
        """Update joke text with specified UUID.
        Return 404 if that UUID not exists of current user is not owner of
        joke with that UUID
        """

        joke_text = parse_joke(request)
        joke_uuid = parse_uuid(uuid)
        logging.info(
            f'User with uuid {user_uuid} created joke {joke}. New joke text "{joke_text}"'
        )
        MJoke.update(uuid=joke_uuid, owner=user_uuid, text=joke_text)
        return "", 200

    @jokes_namespace.doc("delete_joke")
    @jokes_namespace.response(200, "Deleted")
    @jokes_namespace.response(400, "Bad request")
    @jokes_namespace.response(404, "Joke not found")
    @jokes_namespace.response(401, "Unauthorized")
    def delete(self, uuid: str, user_uuid: UUID):
        """Delete joke with specified UUID.
        Return 404 if that UUID not exists of current user is not owner of
        joke with that UUID
        """
        joke_uuid = parse_uuid(uuid)
        logging.info(f"User with uuid {user_uuid} deleted joke with uuid {joke_uuid}")
        MJoke.delete(uuid=joke_uuid, owner=user_uuid)
        return "", 200


jokes_namespace.add_resource(JokesList)
jokes_namespace.add_resource(Joke)
