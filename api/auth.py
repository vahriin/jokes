import logging
from datetime import datetime

from flask_restplus import Resource, Namespace, fields
from flask_jwt_extended import create_access_token
from flask import request
from flask import current_app as app

from werkzeug.exceptions import Unauthorized, NotFound, Conflict

from api.parser import parse_auth_request
from secure.bcrypt import bcrypt
from model.user import User
from model.log import Log
from model.exceptions import UserNotFoundError, UserAlreadyExists


user_namespace = Namespace("user", path="/", description="Authentification operations")

user = user_namespace.model(
    "User",
    {
        "username": fields.String(
            required=True, description="Username of user", example="bob"
        ),
        "password": fields.String(
            required=True, description="User password", example="supersecret"
        ),
    },
)

jwt_token = user_namespace.model(
    "Token",
    {
        "token": fields.String(
            description="JWT access token",
            required=True,
            example="eyJ0eXAiOiJKV1Q------MeJ16YqH05Y",
        ),
        "expired_on": fields.DateTime(
            description="UTC datetime when the token expires",
            required=True,
            example="2019-09-01T18:11:39.359767",
        ),
    },
)


@user_namespace.route("/signin")
class Login(Resource):
    """Provide login operation for existing users"""

    @user_namespace.doc("user_login")
    @user_namespace.response(400, "Bad request")
    @user_namespace.response(404, "User not found")
    @user_namespace.response(401, "Wrong password")
    @user_namespace.expect(user, validate=True)
    @user_namespace.marshal_with(jwt_token)
    def post(self):
        """Return the JWT token for registered user.
        """
        username, password = parse_auth_request(request)
        try:
            user = User.get(username=username)
        except UserNotFoundError as nfe:
            logging.info(f"User with username {username} not found")
            raise NotFound(nfe)

        if bcrypt.check_password_hash(user.password_hash, password):
            now = datetime.utcnow()
            access_token: str = create_access_token(user.uuid, fresh=True)
            Log.create(user_uuid=user.uuid, ip_addr=request.remote_addr)
            logging.info(f"sing in user {user}")
            return {
                "token": access_token,
                "expired_on": now + app.config["JWT_ACCESS_TOKEN_EXPIRES"],
            }
        else:
            logging.info(f"Failed login attempt for user {user}")
            raise Unauthorized(f"Wrong password for username {user.username}")


@user_namespace.route("/signup")
class Signup(Resource):
    """Provide register operation for new users"""

    @user_namespace.response(409, "User with same username already exists")
    @user_namespace.response(400, "Bad request")
    @user_namespace.doc("create_user")
    @user_namespace.expect(user, validate=True)
    @user_namespace.marshal_with(jwt_token, code=201, description="User created")
    def post(self):
        """Register new user with specified username and password.
        Return JWT token for created user.
        Return 409 Conflict if user with that username already exists
        """
        username, password = parse_auth_request(request)
        passhash: str = bcrypt.generate_password_hash(password).decode("utf-8")
        try:
            user = User.create(username=username, passhash=passhash)
            logging.info(f"User with username {username} not found")
        except UserAlreadyExists as uae:
            raise Conflict(uae)

        now = datetime.utcnow()
        access_token: str = create_access_token(user.uuid, fresh=True)
        Log.create(user_uuid=user.uuid, ip_addr=request.remote_addr)
        logging.info(f"sing up user {user}")
        return (
            {
                "token": access_token,
                "expired_on": now + app.config["JWT_ACCESS_TOKEN_EXPIRES"],
            },
            201,
        )


user_namespace.add_resource(Login)
user_namespace.add_resource(Signup)
