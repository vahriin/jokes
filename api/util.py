import requests

from flask import current_app as app

from werkzeug.exceptions import InternalServerError


def get_joke() -> str:
    """Get joke text from external API

    Raises:
        InternalServerError: Any problem with external API

    Returns:
        str -- text of joke
    """
    try:
        responce = requests.get(app.config["EXTERNAL_API"], timeout=5)
        responce.raise_for_status()
        return responce.text.replace('"', "").replace("\n", "")
    except requests.RequestException:
        raise InternalServerError("External API does not respond correctly")
