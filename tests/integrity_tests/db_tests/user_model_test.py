import pytest

from conftest import app_with_database  # noqa

from model.user import User
from model.exceptions import UserAlreadyExists, UserNotFoundError


class TestUserModel:
    @pytest.fixture(autouse=True)
    def _required_app(self, app_with_database):  # noqa
        self._app = app_with_database

    def test_create_and_delete(self):
        user = User.create(username="john", passhash="abcdefg")
        User.delete(username=user.username)

    def test_get(self):
        username = "john"
        user0 = User.create(username=username, passhash="abcdefg")
        user1 = User.get(username=username)
        assert user0 == user1

    def test_duplicate(self):
        username = "bob"
        _ = User.create(username=username, passhash="abcdefg")
        with pytest.raises(UserAlreadyExists):
            User.create(username=username, passhash="blahblah")

    def test_not_found(self):
        with pytest.raises(UserNotFoundError):
            User.get(username="alice")

    def test_delete_not_found(self):
        with pytest.raises(UserNotFoundError):
            User.delete(username="alice")

    def test_str_repr(self):
        user = User.create("patrick", passhash="abc")
        assert str(user) == f"User(uuid={user.uuid}, username={user.username})"
