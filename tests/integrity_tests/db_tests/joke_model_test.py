import pytest
from uuid import uuid4

from conftest import app_with_database  # noqa

from model.user import User
from model.joke import Joke
from model.exceptions import JokeNotFoundError


class TestJokeModel:
    @pytest.fixture(autouse=True)
    def create_user(self, app_with_database):  # noqa
        self.user = User.create(username="john", passhash="abcdefg")
        yield
        User.delete(self.user.username)

    def test_create_and_delete(self):
        joke = Joke.create(owner=self.user.uuid, text="haha")
        Joke.delete(joke.uuid, owner=self.user.uuid)

    def test_get(self):
        joke0 = Joke.create(owner=self.user.uuid, text="haha")
        joke1 = Joke.create(owner=self.user.uuid, text="hahaha")
        joke2 = Joke.get(owner=self.user.uuid, uuid=joke0.uuid)
        assert joke0 == joke2
        jokes = Joke.get_list(owner=self.user.uuid)
        assert len(jokes) == 2
        assert joke0 in jokes
        assert joke1 in jokes
        assert joke2 in jokes

    def test_update(self):
        joke0 = Joke.create(owner=self.user.uuid, text="haha")
        joke0 = Joke.update(uuid=joke0.uuid, owner=self.user.uuid, text="hahaha")
        assert joke0.text == "hahaha"

    def test_not_found(self):
        with pytest.raises(JokeNotFoundError):
            Joke.get(owner=self.user.uuid, uuid=uuid4())

    def test_delete_not_found(self):
        with pytest.raises(JokeNotFoundError):
            Joke.delete(owner=self.user.uuid, uuid=uuid4())

    def test_update_not_found(self):
        with pytest.raises(JokeNotFoundError):
            Joke.update(owner=self.user.uuid, uuid=uuid4(), text="hahaha")

    def test_foreign(self):
        user_that_not_exists = uuid4()
        with pytest.raises(JokeNotFoundError):
            joke = Joke.create(owner=self.user.uuid, text="haha")
            Joke.get(owner=user_that_not_exists, uuid=joke.uuid)
        jokes = Joke.get_list(owner=user_that_not_exists)
        assert len(jokes) == 0

    def test_delete_foreign(self):
        with pytest.raises(JokeNotFoundError):
            joke = Joke.create(owner=self.user.uuid, text="haha")
            Joke.get(owner=uuid4(), uuid=joke.uuid)

    def test_update_foreign(self):
        with pytest.raises(JokeNotFoundError):
            joke = Joke.create(owner=self.user.uuid, text="haha")
            Joke.update(owner=uuid4(), uuid=joke.uuid, text="hahaha")

    def test_str_repr(self):
        joke = Joke.create(owner=self.user.uuid, text="haha")
        assert (
            str(joke) == f"Joke(uuid={joke.uuid}, owner={joke.owner}, text={joke.text})"
        )
