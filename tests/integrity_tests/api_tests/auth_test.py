from pytest_flask.fixtures import client  # noqa

from conftest import create_user, username, password  # noqa


class TestSignup:
    url = "/signup"

    def test_success_signup(self, create_user):  # noqa
        return

    def test_create_no_username(self, client):  # noqa
        data = {"password": "secret"}
        response = client.post(self.url, json=data)
        assert response.status_code == 400

    def test_create_no_password(self, client):  # noqa
        data = {"username": "bob"}
        response = client.post(self.url, json=data)
        assert response.status_code == 400

    def test_same_username(self, client):  # noqa
        data = {"username": username, "password": password}
        response1 = client.post(self.url, json=data)
        assert response1.status_code == 409

    def test_try_to_get(self, client):  # noqa
        response = client.get(self.url)
        assert response.status_code == 405


class TestSignin:
    url = "/signin"

    def test_success_signin(self, create_user, client):  # noqa
        data = {"username": username, "password": password}
        response = client.post(self.url, json=data)
        assert response.status_code == 200
        assert response.mimetype == "application/json"
        resp_data = response.get_json()
        assert resp_data["token"]

    def test_wrong_user_signin(self, create_user, client):  # noqa
        data = {"username": "bill", "password": password}
        response = client.post(self.url, json=data)
        assert response.status_code == 404

    def test_wrong_password_signin(self, create_user, client):  # noqa
        data = {"username": username, "password": password[:-1]}
        response = client.post(self.url, json=data)
        assert response.status_code == 401
