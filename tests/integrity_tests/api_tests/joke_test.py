import pytest
from uuid import uuid4
from pytest_flask.fixtures import client  # noqa

from conftest import create_user  # noqa

jokes_url = "/jokes/"


def test_create_from_geekjokes(create_user, client):  # noqa
    header = {"X-Auth-Token": create_user}
    response = client.post(jokes_url, json={}, headers=header)
    assert response.status_code == 201
    assert response.json


class TestJokeApi:
    jokes = []

    @pytest.fixture(scope="function", autouse=True)
    def create_joke(self, create_user, client):  # noqa
        self.header = {"X-Auth-Token": create_user}
        body = {"text": "hahahahaha"}
        response = client.post(jokes_url, json=body, headers=self.header)
        assert response.status_code == 201
        assert response.json
        self.jokes.append(response.get_json())

    def test_update_joke(self, client):  # noqa
        joke = self.jokes[0]
        joke["text"] = "hehehehehe"
        response0 = client.put(
            f"{jokes_url}{joke['uuid']}", json=joke, headers=self.header
        )
        assert response0.status_code == 200
        response1 = client.get(f"{jokes_url}{joke['uuid']}", headers=self.header)
        assert response1.status_code == 200
        assert joke == response1.get_json()

    def test_update_not_exists(self, client):  # noqa
        response = client.put(
            f"{jokes_url}{uuid4()}", json={"text": "aaaaaa"}, headers=self.header
        )
        assert response.status_code == 404

    def test_update_without_text(self, client):  # noqa
        joke = self.jokes[1]
        response = client.put(
            f"{jokes_url}{joke['uuid']}", json={}, headers=self.header
        )
        assert response.status_code == 400

    def test_delete_joke(self, client):  # noqa
        joke = self.jokes[2]
        response = client.delete(f"{jokes_url}{joke['uuid']}", headers=self.header)
        assert response.status_code == 200

    def test_delete_not_exists(self, client):  # noqa
        response = client.delete(f"{jokes_url}{uuid4()}", headers=self.header)
        assert response.status_code == 404

    def test_get_all_jokes(self, client):  # noqa
        responce = client.get(jokes_url, headers=self.header)
        assert len(responce.json) == 6
