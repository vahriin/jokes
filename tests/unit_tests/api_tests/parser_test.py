import pytest

from uuid import uuid4

from werkzeug.exceptions import BadRequest

from api.parser import parse_uuid


def test_parse_uuid():
    uuid_candidate = str(uuid4())
    _ = parse_uuid(uuid_candidate)
    with pytest.raises(BadRequest):
        _ = parse_uuid(uuid_candidate[:-1])
