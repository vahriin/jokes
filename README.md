# Jokes

Simple API for joke storage


[![pipeline status](https://gitlab.com/vahriin/jokes/badges/master/pipeline.svg)](https://gitlab.com/vahriin/jokes/commits/master)

## How to run:

Change the SECRET_KEY and JWT_HEADER_TYPE in `config/init.py`

Run

```
docker-compose up
docker-compose exec server flask db upgrade
```

Do not run on production! Use WSGI server instead.

## API documentation

The API documentation stored on http://localhost:5000.
