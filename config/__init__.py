from datetime import timedelta

DEBUG = True

SQLALCHEMY_DATABASE_URI = "postgresql://postgres:123@postgres:5432/postgres"
SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_ECHO = False
SECRET_KEY = "CHANGE ME"

JWT_TOKEN_LOCATION = ["headers"]
JWT_ACCESS_TOKEN_EXPIRES = timedelta(hours=1)
JWT_SECRET_KEY = "AND ME PLZ"
JWT_HEADER_NAME = "X-Auth-Token"
JWT_HEADER_TYPE = ""

EXTERNAL_API = "https://geek-jokes.sameerkumar.website/api"
