import logging

from flask import Flask
from flask_migrate import Migrate


def create_app():
    """App factory
    """
    app = Flask(__name__)
    app.config.from_object("config")

    if app.config.get("DEBUG", False):
        logging.basicConfig(level=logging.INFO)
    else:
        logging.basicConfig(level=logging.WARNING)

    from datastore.database import db

    db.init_app(app)
    migrate = Migrate(app, db)  # noqa

    from secure.jwt import jwt

    jwt.init_app(app)

    from secure.bcrypt import bcrypt

    bcrypt.init_app(app)

    from api.joke import jokes_namespace
    from api.auth import user_namespace
    from api import api

    api.add_namespace(jokes_namespace)
    api.add_namespace(user_namespace)

    api.init_app(app)
    return app
