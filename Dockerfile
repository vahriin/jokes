FROM python:3.7-slim

COPY requirements.txt /requirements.txt
RUN pip install -r /requirements.txt --no-cache-dir

ADD . /app

WORKDIR /app

ENV FLASK_APP="app:create_app()"

CMD [ "flask", "run", "--host", "0.0.0.0"]