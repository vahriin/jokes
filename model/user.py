from uuid import uuid4

from sqlalchemy.dialects.postgresql import UUID as UUID_POSTGRES
from sqlalchemy.exc import IntegrityError

from datastore.database import db
from model.exceptions import UserNotFoundError, UserAlreadyExists


class User(db.Model):  # noqa
    __tablename__ = "users"

    uuid = db.Column(UUID_POSTGRES(as_uuid=True), primary_key=True)
    username = db.Column(db.String, unique=True, index=True)
    password_hash = db.Column(db.String, nullable=False)

    def __init__(self, username: str, passhash: str):
        self.uuid = uuid4()
        self.username = username
        self.password_hash = passhash

    @classmethod
    def create(cls, username: str, passhash: str):
        """Create and put user to table

        Raises:
            UserAlreadyExists: user with specified username already exists

        Returns:
            User -- User instance with specified username
        """
        user = cls(username=username, passhash=passhash)
        db.session.add(user)
        try:
            db.session.commit()
        except IntegrityError:
            raise UserAlreadyExists(id_=username)
        return user

    @classmethod
    def get(cls, username: str):
        """Return user with specified username

        Raises:
            UserNotFoundError: user with specified username not found

        Returns:
            User -- User instance with specified username
        """
        user = db.session.query(cls).filter(cls.username == username).first()
        if not user:
            raise UserNotFoundError(id_=username)
        return user

    @classmethod
    def delete(cls, username: str):
        """Delete user by specified username

        Raises:
            UserNotFoundError: User not found
        """
        affected_rows = db.session.query(cls).filter(cls.username == username).delete()
        if affected_rows:
            db.session.commit()
            return
        raise UserNotFoundError(id_=username)

    def __str__(self):
        return f"User(uuid={self.uuid}, username={self.username})"
