class BaseError(Exception):
    """
    Provide human readable error.
    """

    def __init__(self, action: str, type_: str, access_type: str, id_: str):
        self.action: str = action
        self.type_: str = type_
        self.access_type: str = access_type
        self.id_: str = id_

    def __str__(self):
        return f"{self.type_} with {self.access_type} {self.id_} {self.action}"


class NotFoundError(BaseError):
    """Base error for NotFoundError exceptions"""

    def __init__(self, type_: str, access_type: str, id_: str):
        super().__init__(
            action="not found", type_=type_, access_type=access_type, id_=id_
        )


class UserNotFoundError(NotFoundError):
    def __init__(self, id_: str, access_type: str = "username"):
        super().__init__(type_="User", access_type=access_type, id_=id_)


class JokeNotFoundError(NotFoundError):
    def __init__(self, id_: str, access_type: str = "uuid"):
        super().__init__(type_="Joke", access_type=access_type, id_=id_)


class AlreadyExistsError(BaseError):
    """Base error for AlreadyExists exceptions"""

    def __init__(self, type_: str, access_type: str, id_: str):
        super().__init__(
            action="already exists", type_=type_, access_type=access_type, id_=id_
        )


class UserAlreadyExists(AlreadyExistsError):
    def __init__(self, id_: str, access_type: str = "username"):
        super().__init__(type_="User", access_type=access_type, id_=id_)
