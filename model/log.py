from uuid import UUID, uuid4
from datetime import datetime

from sqlalchemy.dialects.postgresql import UUID as UUID_POSTGRES, INET

from datastore.database import db
from model.user import User


class Log(db.Model):  # noqa
    """Table for store the loglines
    """

    __tablename__ = "logs"

    uuid = db.Column(UUID_POSTGRES(as_uuid=True), primary_key=True)
    user_uuid = db.Column(
        UUID_POSTGRES(as_uuid=True),
        db.ForeignKey(User.uuid, ondelete="CASCADE"),
        nullable=False,
    )
    ip_addr = db.Column(INET, nullable=False)
    request_datetime = db.Column(db.DateTime, nullable=False)

    def __init__(self, user_uuid: UUID, ip_addr: str, request_datetime: datetime):
        self.uuid = uuid4()
        self.user_uuid = user_uuid
        self.ip_addr = ip_addr
        self.request_datetime = request_datetime

    @classmethod
    def create(cls, user_uuid: UUID, ip_addr: str):
        entry = cls(
            user_uuid=user_uuid, ip_addr=ip_addr, request_datetime=datetime.utcnow()
        )
        db.session.add(entry)
        db.session.commit()
