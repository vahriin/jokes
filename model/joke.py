from uuid import UUID, uuid4

from sqlalchemy.dialects.postgresql import UUID as UUID_POSTGRES

from datastore.database import db
from model.user import User
from model.exceptions import JokeNotFoundError


class Joke(db.Model):  # noqa
    """Database model for store joke.

    uuid: Unique ID of entry
    owner: UUID of user that joke owner
    text: Text of joke
    """

    __tablename__ = "jokes"

    uuid = db.Column(UUID_POSTGRES(as_uuid=True), primary_key=True)
    owner = db.Column(
        UUID_POSTGRES(as_uuid=True),
        db.ForeignKey(User.uuid, ondelete="CASCADE"),
        nullable=False,
    )
    text = db.Column(db.Text, nullable=False)

    def __init__(self, owner: UUID, text: str):
        self.uuid = uuid4()
        self.owner = owner
        self.text = text

    @classmethod
    def get(cls, uuid: UUID, owner: UUID):
        """Return joke by specified uuid and owner uuid

        Raises:
            JokeNotFoundError: raises if result is empty

        Returns:
            Joke -- Joke instance
        """
        joke = (
            db.session.query(cls).filter(cls.uuid == uuid, cls.owner == owner).first()
        )
        if not joke:
            raise JokeNotFoundError(id_=str(uuid))
        return joke

    @classmethod
    def get_list(cls, owner: UUID):
        """Return list of jokes belongs to specified user.
        If user has no jokes, return empty list

        Returns:
            List[Joke] -- list of jokes
        """
        return db.session.query(cls).filter(cls.owner == owner).all()

    @classmethod
    def create(cls, owner: UUID, text: str):
        """Create and put joke to table

        Returns:
            Joke -- created Joke
        """
        joke = cls(owner=owner, text=text)
        db.session.add(joke)
        db.session.commit()
        return joke

    @classmethod
    def delete(cls, uuid: UUID, owner: UUID):
        """Delete specified Joke

        Raises:
            JokeNotFoundError: Joke with specified UUID not exists or not owned by current user
    """
        affected_rows = (
            db.session.query(cls).filter(cls.uuid == uuid, cls.owner == owner).delete()
        )
        if affected_rows:
            db.session.commit()
            return
        raise JokeNotFoundError(id_=str(uuid))

    @classmethod
    def update(cls, uuid: UUID, owner: UUID, text: str):
        """Update joke text by specified text

        Raises:
            JokeNotFoundError: oke with specified UUID not exists or not owned by current user
    """
        joke = (
            db.session.query(cls).filter(cls.uuid == uuid, cls.owner == owner).first()
        )
        if not joke:
            raise JokeNotFoundError(id_=str(uuid))
        joke.text = text
        db.session.add(joke)
        db.session.commit()
        return joke

    def __str__(self):
        return f"Joke(uuid={self.uuid}, owner={self.owner}, text={self.text})"
