import pytest

from datastore.database import db
from app import create_app


@pytest.fixture(scope="session")
def app():
    app = create_app()

    return app


@pytest.fixture(scope="module")
def app_with_database(app):
    db.create_all(app=app)
    yield app
    db.session.remove()
    db.drop_all(app=app)


username = "bob"
password = "secret"


@pytest.fixture(scope="module")
def create_user(app_with_database):  # noqa
    data = {"username": username, "password": password}
    with app_with_database.test_client() as client:
        response = client.post("/signup", json=data)
        assert response.status_code == 201
        assert response.mimetype == "application/json"
        resp_data = response.get_json()
        assert resp_data["token"]
    return f"{resp_data['token']}"


@pytest.fixture(scope="class")
def login_user(app_with_database):
    data = {"username": username, "password": password}
    with app_with_database.test_client() as client:
        response = client.post("/signin", json=data)
        assert response.status_code == 200
        assert response.mimetype == "application/json"
        resp_data = response.get_json()
        assert resp_data["token"]
    return resp_data["token"]
